﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RieeEngine
{
    /// <summary>
    /// This class adds more functionality to Many classes.
    /// 
    /// List of clases:
    /// 
    /// Random
    /// Vector2
    /// </summary>
    public static class Extensions
    {
        public static float toAngle(this Vector2 vector)
        {
            return (float)Math.Atan2(vector.Y, vector.X);
        }

        public static Vector2 scaleTo(this Vector2 vector, float length)
        {
            return vector * (length / vector.Length());
        }

        public static Point toPoint(this Vector2 vector)
        {
            return new Point((int)vector.X, (int)vector.Y);
        }

        public static float nextFloat(this Random rand, float minValue, float maxValue)
        {
            return (float)rand.NextDouble() * (maxValue - minValue) + minValue;
        }

        public static Vector2 nextVector2(this Random rand, float minLength, float maxLength)
        {
            double theta = rand.NextDouble() * 2 * Math.PI;
            float length = rand.nextFloat(minLength, maxLength);
            return new Vector2(length * (float)Math.Cos(theta), length * (float)Math.Sin(theta));
        }

        public static int[] randomNoRepeatedArray(this Random rand, int n) // Retorna arreglo de tamaño n con números aleatorios que no se repiten
        {
            int[] nums = new int[n];
            for (int i = 0; i < n; i++)
                nums[i] = i;
            rand = new Random();
            nums = rand.shuffleArray(nums);
            return nums;
        }

        public static int[] shuffleArray(this Random rand , int[] array)
        {
            for (int t = 0; t < array.Length; t++)
            {
                int tmp = array[t];
                int r = rand.Next(t, array.Length);
                array[t] = array[r];
                array[r] = tmp;
            }

            return array;
        }

        public static Vector2 vectorScale(this Vector2 position,Vector2 _vector, float _percentageX, float _percentageY, int _screenWidth, int _screenHeight, Vector2 pointerSize)
        {
             float _x1 = _screenWidth * _percentageX;     // Center the X side of rectangle      //  y1------------------y2
            _x1 = (_screenWidth - _x1) / 2;                                                     //   \                  /
                                                                                                //    \                /
            float _x2 = _screenWidth - _x1;                                                     //     x1------------x2

            float _slopeX = _screenWidth / (_x2 - _x1);  // slope = y2-y1 / x2-x1
            _vector.X = (_slopeX * _vector.X) - (_slopeX * _x1);     // slope * (x - x1)


            float _x1Y = _screenHeight * _percentageY;     // Center the Y side of rectangle
            _x1Y = (_screenHeight - _x1Y) / 2;

            float _x2Y = _screenHeight - _x1Y;

            float _slopeY = _screenHeight / (_x2Y - _x1Y);  // slope = y2-y1 / x2-x1
            _vector.Y = (_slopeY * _vector.Y) - (_slopeY * _x1Y);     // slope * (x - x1)

            if (_vector.X >= _screenWidth - pointerSize.X)
                _vector.X = _screenWidth - pointerSize.X;

            if (_vector.X <= 0)
                _vector.X = 0;

            if (_vector.Y >= _screenHeight - pointerSize.Y)
                _vector.Y = _screenHeight - pointerSize.Y;

            if (_vector.Y <= 0)
                _vector.Y = 0;

            position = _vector;

            return position;
        }

        //-----Bezier curves-----------------------------------------------------------------------------
        public static Vector2 getNextBezierPoint(this Vector2 vector,float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
        {
            float cx = 3 * (p1.X - p0.X);
            float cy = 3 * (p1.Y - p0.Y);

            float bx = 3 * (p2.X - p1.X) - cx;
            float by = 3 * (p2.Y - p1.Y) - cy;

            float ax = p3.X - p0.X - cx - bx;
            float ay = p3.Y - p0.Y - cy - by;

            float Cube = t * t * t;
            float Square = t * t;

            float resX = (ax * Cube) + (bx * Square) + (cx * t) + p0.X;
            float resY = (ay * Cube) + (by * Square) + (cy * t) + p0.Y;

            vector = new Vector2(resX, resY);

            return vector;
        }
    }
}
