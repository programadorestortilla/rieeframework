﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RieeEngine.Kinect;

namespace RieeEngine
{
    public static class Input
    {
        /// <summary>
        /// This class is for debugging only. Captures every single mouse event.
        /// </summary>
        private static KeyboardState keyboardState, lastKeyboardState;
        private static MouseState mouseState , lastMouseState;
        public static Vector2 MousePosition
        {
            get
            {
                return new Vector2(mouseState.X, mouseState.Y);
            }
        }

        /// <summary>
        /// Updates the class so it can capture the events.
        /// </summary>
        public static void update()
        {
            lastMouseState = mouseState;
            lastKeyboardState = keyboardState;
            mouseState = Mouse.GetState();
            keyboardState = Keyboard.GetState();
        }

        /// <summary>
        /// Checks if the Left Mouse Button was click.
        /// </summary>
        /// <returns>Returns if the button was clicked or not.</returns>
        public static bool wasButtonPressed()
        {
            return mouseState.LeftButton == ButtonState.Pressed && lastMouseState.LeftButton == ButtonState.Released;
        }

        /// <summary>
        /// Checks if the Left Mouse Button is pressed
        /// </summary>
        /// <returns>Returns if the buttons is being pressed or not</returns>
        public static bool isLeftButtonPressed()
        {
            return mouseState.LeftButton == ButtonState.Pressed;
        }

        /// <summary>
        /// Checks if the Right Mouse Button is pressed
        /// </summary>
        /// <returns>Return if the button is being pressed or not</returns>
        public static bool isRightButtonPressed()
        {
            return mouseState.RightButton == ButtonState.Pressed;
        }

        /// <summary>
        /// Checks if the Up keyboad key was pressed
        /// </summary>
        /// <returns></returns>
        public static bool upKeyboardKeyWasPressed()
        {
            return keyboardState.IsKeyDown(Keys.Up) && lastKeyboardState.IsKeyUp(Keys.Up);
        }

        /// <summary>
        /// Checks if the Dow keyboad key was pressed
        /// </summary>
        /// <returns></returns>
        public static bool downKeyboardKeyWasPressed()
        {
            return keyboardState.IsKeyDown(Keys.Down) && lastKeyboardState.IsKeyUp(Keys.Down);
        }

        /// <summary>
        /// Checks if the Left keyboad key was pressed
        /// </summary>
        /// <returns></returns>
        public static bool leftKeyboardKeyWasPressed()
        {
            return keyboardState.IsKeyDown(Keys.Left) && lastKeyboardState.IsKeyUp(Keys.Left);
        }

        /// <summary>
        /// Checks if the Right keyboad key was pressed
        /// </summary>
        /// <returns></returns>
        public static bool rightKeyboardKeyWasPressed()
        {
            return keyboardState.IsKeyDown(Keys.Right) && lastKeyboardState.IsKeyUp(Keys.Right);
        }

        /// <summary>
        /// Under development
        /// </summary>
        /// <returns>Returns the position</returns>
        public static Vector2 getMovementDirectionKeyBoard()
        {
            Vector2 direction = new Vector2();

            if (keyboardState.IsKeyDown(Keys.A))
                direction.X -= 1;
            if (keyboardState.IsKeyDown(Keys.D))
                direction.X += 1;
            if (keyboardState.IsKeyDown(Keys.W))
                direction.Y -= 1;
            if (keyboardState.IsKeyDown(Keys.S))
                direction.Y += 1;

            return direction;
        }
    }
}
