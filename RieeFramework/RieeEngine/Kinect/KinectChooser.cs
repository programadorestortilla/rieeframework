﻿namespace RieeEngine.Kinect
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Microsoft.Kinect;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// This class will pick a kinect sensor if available.
    /// </summary>
    public class KinectChooser : DrawableGameComponent
    {
        /// <summary>
        /// The status to string mapping.
        /// </summary>
        private readonly Dictionary<KinectStatus, string> statusMap = new Dictionary<KinectStatus, string>();

        /// <summary>
        /// The chooser background texture.
        /// </summary>
        private Texture2D chooserBackground;

        /// <summary>
        /// The SpriteBatch used for rendering.
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// The font for rendering the state text.
        /// </summary>
        private SpriteFont font;

        /// <summary>
        /// The requested color image format.
        /// </summary>
        private readonly ColorImageFormat colorImageFormat;

        /// <summary>
        /// The requested depth image format.
        /// </summary>
        private readonly DepthImageFormat depthImageFormat;


        /// <summary>
        /// The class that renders the color stream data.
        /// </summary>
        public readonly ColorStreamRenderer colorStreamRenderer;

        /// <summary>
        /// The class that renders the depth stream data.
        /// </summary>
        public readonly DepthStreamRenderer depthStreamRenderer;


        /// <summary>
        /// The class that reads out the InteractionStream information;
        /// </summary>
        public InteractionStreamReader interactionStream { get; private set;}

        /// <summary>
        /// The flag that controls the rendering
        /// </summary>
        private bool isRendering;
        /// <summary>
        /// Initializes a new Instance of the Kinect Chooser.This one only looks for a Sensor Available 
        /// </summary>
        /// <param name="game"></param>
        public KinectChooser(Game game)
            : base(game)
        {
            KinectSensor.KinectSensors.StatusChanged += this.KinectSensors_StatusChanged;

            this.DiscoverSensor();

            this.statusMap.Add(KinectStatus.Connected, string.Empty);
            this.statusMap.Add(KinectStatus.DeviceNotGenuine, "Device Not Genuine");
            this.statusMap.Add(KinectStatus.DeviceNotSupported, "Device Not Supported");
            this.statusMap.Add(KinectStatus.Disconnected, "Required");
            this.statusMap.Add(KinectStatus.Error, "Error");
            this.statusMap.Add(KinectStatus.Initializing, "Initializing...");
            this.statusMap.Add(KinectStatus.InsufficientBandwidth, "Insufficient Bandwidth");
            this.statusMap.Add(KinectStatus.NotPowered, "Not Powered");
            this.statusMap.Add(KinectStatus.NotReady, "Not Ready");

        }
        /// <summary>
        /// Initializes a new instance of the Kinect Chooser with the ability to render the RGBStream and the DepthStream .
        /// </summary>
        /// <param name="game">The Game object</param>
        /// <param name="isColorCameraActive">Read the data stream of the RGB camera</param>
        /// <param name="isDepthCameraActive">Read the data stream of the depth camera</param>
        public KinectChooser(Game game, bool isColorCameraActive, bool isDepthCameraActive)
            : base(game)
        {

            if (isColorCameraActive)
            {
                this.colorImageFormat = ColorImageFormat.RgbResolution640x480Fps30;
                this.colorStreamRenderer = new ColorStreamRenderer(game);
            }
            else
                this.colorImageFormat = ColorImageFormat.Undefined;

            if (isDepthCameraActive)
            {
                this.depthImageFormat = DepthImageFormat.Resolution640x480Fps30;
                this.depthStreamRenderer = new DepthStreamRenderer(game);
                isRendering = true;
            }
            else
                this.depthImageFormat = DepthImageFormat.Undefined;

            KinectSensor.KinectSensors.StatusChanged += this.KinectSensors_StatusChanged;

            this.DiscoverSensor();

            this.statusMap.Add(KinectStatus.Connected, string.Empty);
            this.statusMap.Add(KinectStatus.DeviceNotGenuine, "Device Not Genuine");
            this.statusMap.Add(KinectStatus.DeviceNotSupported, "Device Not Supported");
            this.statusMap.Add(KinectStatus.Disconnected, "Required");
            this.statusMap.Add(KinectStatus.Error, "Error");
            this.statusMap.Add(KinectStatus.Initializing, "Initializing...");
            this.statusMap.Add(KinectStatus.InsufficientBandwidth, "Insufficient Bandwidth");
            this.statusMap.Add(KinectStatus.NotPowered, "Not Powered");
            this.statusMap.Add(KinectStatus.NotReady, "Not Ready");
        }

        /// <summary>
        /// Initializes a new instance of the Kinect Chooser with Kinect Interactions Enabled and DepthStreamRenderer and SkeletonRenderer Available.
        /// </summary>
        /// <param name="game">The Game Object</param>
        /// <param name="interactionClient">The InteractionClient Parameters</param>
        public KinectChooser(Game game, InteractionClient interactionClient, bool isRenderingStream)
            : base(game)
        {
            this.isRendering = isRenderingStream;
            this.depthImageFormat = DepthImageFormat.Resolution640x480Fps30;
            this.depthStreamRenderer = new DepthStreamRenderer(game);

            KinectSensor.KinectSensors.StatusChanged += this.KinectSensors_StatusChanged;

            this.DiscoverSensor();

            /*
             * DEVELOPER NOTe
             * 
             * Well well, my dear watson. here's the first part of the solution of the interacionStreamReader Nullpointer-Shit.
             * 
             * IT's simple. If we don't find a kinect Sensor either connected or working then don't initilize the interactionStream!!
             * So that's why we are copying this little items (the game instance and the interactionClient information)
             * 
             * The 2nd part of the solution it's down below (don't be lazy , scroll down a little bit).
             */
            //We are gonna copy this, in case the kinect sensor it's not plugged in when the first time the app starts
            if (this.Sensor != null)
                this.interactionStream = new InteractionStreamReader(game, interactionClient, this.Sensor);
            else
            {
                this.game = game;
                this.interactionClient = interactionClient;
            }

            this.statusMap.Add(KinectStatus.Connected, string.Empty);
            this.statusMap.Add(KinectStatus.DeviceNotGenuine, "Device Not Genuine");
            this.statusMap.Add(KinectStatus.DeviceNotSupported, "Device Not Supported");
            this.statusMap.Add(KinectStatus.Disconnected, "Required");
            this.statusMap.Add(KinectStatus.Error, "Error");
            this.statusMap.Add(KinectStatus.Initializing, "Initializing...");
            this.statusMap.Add(KinectStatus.InsufficientBandwidth, "Insufficient Bandwidth");
            this.statusMap.Add(KinectStatus.NotPowered, "Not Powered");
            this.statusMap.Add(KinectStatus.NotReady, "Not Ready");
        }

        /// <summary>
        /// Game class. This purpose of this it's only to backup the class if it crashes.
        /// it's better if the class crashes out and not the whole app.
        /// </summary>
        private Game game;

        /// <summary>
        /// Same with this. This purpose of this it's only to backup the class if it crashes.
        /// it's better if the class crashes out and not the whole app.
        /// </summary>
        private InteractionClient interactionClient;


        /*
         * This is the little patchie :)
         */
        private void ReloadInteracionStream()
        {
            this.interactionStream = new InteractionStreamReader(game, interactionClient, this.Sensor);
        }

        /// <summary>
        /// Gets the selected KinectSensor.
        /// </summary>
        public KinectSensor Sensor { get; private set; }

        /// <summary>
        /// Gets the last known status of the KinectSensor.
        /// </summary>
        public KinectStatus LastStatus { get; private set; }

        /// <summary>
        /// This method initializes necessary objects.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();

            this.spriteBatch = new SpriteBatch(Game.GraphicsDevice);
        }

        /// <summary>
        /// Updates the KinectChooser
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // If the sensor is not found, not running, or not connected, stop now
            if (null == this.Sensor ||
                false == this.Sensor.IsRunning ||
                KinectStatus.Connected != this.Sensor.Status)
            {
                return;
            }

            if (null != colorStreamRenderer)
                this.colorStreamRenderer.Update(gameTime);


            if (null != depthStreamRenderer)
                this.depthStreamRenderer.Update(gameTime);

            if (KinectStatus.Initializing == this.Sensor.Status)
                return;


            /*
             * DEVELOPER NOTE
             * 
             * This is the 2nd part of the solution i talked you about in the INteractionStreamReader Update method
             * 
             * the solution is; if there's no sensor and the other stream are not yet initialized , well, then reload the
             * InteractionStream which it has not been initializaed in one of the Contructors of this class. (There's the 1st part).
             */
            if (this.interactionStream == null && this.Sensor.Status == KinectStatus.Connected)
                ReloadInteracionStream();

            if (null != interactionStream && null!= depthStreamRenderer && 
                depthStreamRenderer.isDepthStreamReady)
              interactionStream.Update(depthStreamRenderer.skeletonStream.SkeletonFrame,
                  depthStreamRenderer.skeletonStream.skeletonData,
                  depthStreamRenderer.skeletonStream.AccelerometerCurrentReading,
                  depthStreamRenderer.RawFrameData,
                  depthStreamRenderer.TimeStamp
                  );

            /*
             * DEVELOPER NOTE
             * 
             * This Code below, changes the angle of the kinect camera. It can be used anytime. but it will throw
             * and exception. Why?!. well it's because the kinect gear it's not made to be used constantly, so you
             * have to find a way to prevent it's constant use, but that expetion it's totally normal to happen. 
             * 
             * It is recommended to call this funcion every 20 seconds or once per second. if you surpass those parameters
             * an exception is thrown and that one, is the one you have to prevent.
             * 
             * So congrats you are a ninja programmer now!.
             * 
             * 
             * OOOOhhhh i forgot something. See those Try and Catch? well that's a quick fix to prevent your app to crash. 
             * Those capture the exception i talked you about and then nothing happens( No crashing application =) ).
             */
            var elevationAngle = 0;
            try
            {
                elevationAngle = Sensor.ElevationAngle;
            }
            catch(InvalidOperationException) { }
            if (Input.downKeyboardKeyWasPressed())
            {
                try
                {
                    elevationAngle -= 5;
                    if (elevationAngle <= Sensor.MinElevationAngle +2)
                        elevationAngle = -25;
                    Sensor.ElevationAngle = elevationAngle;
                }
                catch (InvalidOperationException)
                {
                    //Do nothing if something goes wrong
                }
            }
            if (Input.upKeyboardKeyWasPressed())
                {
                    try
                    {
                        elevationAngle += 5;
                        if (elevationAngle >= Sensor.MaxElevationAngle-2)
                            elevationAngle = 25;
                        Sensor.ElevationAngle = elevationAngle;
                    }
                    catch (InvalidOperationException)
                    {
                        //Do nothing if something goes wrong
                    }
                }
            if (Input.leftKeyboardKeyWasPressed())
                try
                {
                    Sensor.ElevationAngle = -25;
                }
                catch (InvalidOperationException) { }
            if (Input.rightKeyboardKeyWasPressed())
                try
                {
                    Sensor.ElevationAngle = 25;
                }
                catch (InvalidOperationException) { }
        }

        /// <summary>
        /// This method renders the current state of the KinectChooser.
        /// </summary>
        /// <param name="gameTime">The elapsed game time.</param>
        public override void Draw(GameTime gameTime)
        {
            // If the spritebatch is null, call initialize
            if (this.spriteBatch == null)
            {
                this.Initialize();
            }

            // If the background is not loaded, load it now
            if (this.chooserBackground == null)
            {
                this.LoadContent();
            }

            // If we don't have a sensor, or the sensor we have is not connected
            // then we will display the information text
            if (this.Sensor == null || this.LastStatus != KinectStatus.Connected)
            {
                this.spriteBatch.Begin();

                // Render the background
                this.spriteBatch.Draw(
                    this.chooserBackground,
                    new Vector2(Game.GraphicsDevice.Viewport.Width / 2, Game.GraphicsDevice.Viewport.Height / 2),
                    null,
                    Color.White,
                    0,
                    new Vector2(this.chooserBackground.Width / 2, this.chooserBackground.Height / 2),
                    1,
                    SpriteEffects.None,
                    0);

                // Determine the text
                string txt = "Required";
                if (this.Sensor != null)
                {
                    txt = this.statusMap[this.LastStatus];
                }

                // Render the text
                Vector2 size = this.font.MeasureString(txt);
                this.spriteBatch.DrawString(
                    this.font,
                    txt,
                    new Vector2((Game.GraphicsDevice.Viewport.Width - size.X) / 2, (Game.GraphicsDevice.Viewport.Height / 2) + size.Y),
                    Color.White);
                this.spriteBatch.End();
            }
            else
            {
                if (null != colorStreamRenderer)
                    this.colorStreamRenderer.Draw(gameTime);


                if (null != depthStreamRenderer && isRendering)
                    this.depthStreamRenderer.Draw(gameTime);
            }
            base.Draw(gameTime);
        }

        /// <summary>
        /// This method loads the textures and fonts.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();

            this.chooserBackground = Game.Content.Load<Texture2D>("RieeEngine/ChooserBackground");
            this.font = Game.Content.Load<SpriteFont>("RieeEngine/Segoe16");

            if (null != colorStreamRenderer)
                this.colorStreamRenderer.LoadContent(Game.Content);


            if (null != depthStreamRenderer)
                this.depthStreamRenderer.LoadContent(Game.Content);
        }

        /// <summary>
        /// This method ensures that the KinectSensor is stopped before exiting.
        /// </summary>
        protected override void UnloadContent()
        {
            base.UnloadContent();

            // Always stop the sensor when closing down
            if (this.Sensor != null)
            {
                this.Sensor.Stop();
            }
        }

        /// <summary>
        /// This method will use basic logic to try to grab a sensor.
        /// Once a sensor is found, it will start the sensor with the
        /// requested options.
        /// </summary>
        private void DiscoverSensor()
        {
            // Grab any available sensor
            this.Sensor = KinectSensor.KinectSensors.FirstOrDefault();

            var smoothParameters = new TransformSmoothParameters
            {
                Smoothing = 0.75f,          //Smoothing = 0.5f,
                Correction = 0.1f,          //Correction = 0.5f,
                Prediction = 0.0f,          //Prediction = 0.5f,
                JitterRadius = 0.05f,       //JitterRadius = 0.05f,
                MaxDeviationRadius = 0.04f  //MaxDeviationRadius = 0.04f
            };

            if (this.Sensor != null)
            {
                this.LastStatus = this.Sensor.Status;

                // If this sensor is connected, then enable it
                if (this.LastStatus == KinectStatus.Connected)
                {
                    try
                    {
                        this.Sensor.SkeletonStream.Enable();
                        try
                        {
                            this.Sensor.Start();
                            this.Sensor.SkeletonStream.Enable(smoothParameters);
                            if(this.colorImageFormat != ColorImageFormat.Undefined)
                                this.Sensor.ColorStream.Enable(this.colorImageFormat);
                            if (this.depthImageFormat != DepthImageFormat.Undefined)
                                this.Sensor.DepthStream.Enable(this.depthImageFormat);
                        }
                        catch (IOException)
                        {
                            // sensor is in use by another application
                            // will treat as disconnected for display purposes
                            this.Sensor = null;
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        // KinectSensor might enter an invalid state while
                        // enabling/disabling streams or stream features.
                        // E.g.: sensor might be abruptly unplugged.
                        this.Sensor = null;
                    }
                }
            }
            else
            {
                this.LastStatus = KinectStatus.Disconnected;
            }
        }

        /// <summary>
        /// This wires up the status changed event to monitor for 
        /// Kinect state changes.  It automatically stops the sensor
        /// if the device is no longer available.
        /// </summary>
        /// <param name="sender">The sending object.</param>
        /// <param name="e">The event args.</param>
        private void KinectSensors_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            // If the status is not connected, try to stop it
            if (e.Status != KinectStatus.Connected)
            {
                e.Sensor.Stop();
            }

            this.LastStatus = e.Status;
            this.DiscoverSensor();
        }

        /// <summary>
        /// This method is used to map the SkeletonPoint to the color frame.
        /// </summary>
        /// <param name="point">The SkeletonPoint to map.</param>
        /// <returns>A Vector2 of the location on the color frame.</returns>
        private Vector2 SkeletonToColorMap(SkeletonPoint point)
        {
            if ((null != Sensor) && (null != Sensor.ColorStream))
            {
                // This is used to map a skeleton point to the color image location
                var colorPt = Sensor.CoordinateMapper.MapSkeletonPointToColorPoint(point, Sensor.ColorStream.Format);
                return new Vector2(colorPt.X, colorPt.Y);
            }

            return Vector2.Zero;
        }
    }
}
