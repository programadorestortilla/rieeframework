﻿namespace RieeEngine.Kinect
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Kinect.Toolkit.Interaction;
    using Microsoft.Xna.Framework;
    using Microsoft.Kinect;

    public class InteractionStreamReader:DrawableScreen
    {
        private InteractionStream interactionStream;
        private InteractionClient interactionClient;

        private UserInfo[] userInfos;

        private Dictionary<int, InteractionHandEventType> lastLeftHandEvents = new Dictionary<int, InteractionHandEventType>();
        private Dictionary<int, InteractionHandEventType> lastRightHandEvents = new Dictionary<int, InteractionHandEventType>();

        public List<Hand> CapturedHands { get; private set; }

        public InteractionStreamReader(Game game, InteractionClient interactionClient, KinectSensor Sensor)
            : base(game)
        {
            this.interactionClient = interactionClient;
            if (Sensor == null)
                return;
            this.interactionStream = new InteractionStream(Sensor, this.interactionClient);
            this.userInfos = new UserInfo[InteractionFrame.UserInfoArrayLength];
            this.interactionStream.InteractionFrameReady += this.InteractionStreamOnInteractionFrameReady;
            this.CapturedHands = new List<Hand>();
        }

        /// <summary>
        /// Updates the InteractionStream
        /// </summary>
        /// <param name="skeletonFrame">The last SkeletonFrame Captured</param>
        /// <param name="skeleton">The last Skeleton Array Captured</param>
        /// <param name="accelerometerCurrentReading">The last AccelerometerReading</param>
        /// <param name="rawPixelData">The RawPixelData from the depth camera</param>
        /// <param name="timeStamp">The last timeStamp captured</param>
        public void Update(SkeletonFrame skeletonFrame, Skeleton[] skeleton, 
            Microsoft.Kinect.Vector4 accelerometerCurrentReading,
            DepthImagePixel[] rawPixelData,
            long timeStamp)
        {
            // If the sensor is not found, not running, or not connected, stop now
            if (null == this.Chooser.Sensor ||
                false == this.Chooser.Sensor.IsRunning ||
                KinectStatus.Connected != this.Chooser.Sensor.Status)
            {
                return;
            }

            if (null == skeletonFrame ||
                0 == skeleton.Count() ||
                accelerometerCurrentReading == null ||
                null == rawPixelData || 
                timeStamp == 0)
            {
                return;
            }

            
            /*
             * DEVELOPER NOTE
             * 
             * Here we have a problem. it throws an NullPointer Exception. why? well i dont't know , you have to figured that out
             * I put a little patchie in the Kinect Chooser class, but i don't think it's a good way to solve this problem
             * it solves it but i'm not convinced about it. 
             * 
             * Don't forget to look for an optimous solution of that shit!!.
             * 
             * Congrats the 1st version it's complete
             * 
             * Here takes this slice of pizza **the developer gives you a slice of pepperoni pizza with mushrooms**
             */

            //Process the data received in order to calculate the stream information
            try
            {
                interactionStream.ProcessDepth(rawPixelData, timeStamp);
                interactionStream.ProcessSkeleton(skeleton,accelerometerCurrentReading,skeletonFrame.Timestamp);
            }
            catch (InvalidOperationException) { }
 
        }

        private void InteractionStreamOnInteractionFrameReady(object sender, InteractionFrameReadyEventArgs args)
        {
            // If the sensor is not found, not running, or not connected, stop now
            if (null == this.Chooser.Sensor ||
                false == this.Chooser.Sensor.IsRunning ||
                KinectStatus.Connected != this.Chooser.Sensor.Status)
            {
                return;
            }

            using (var iaf = args.OpenInteractionFrame()) //dispose as soon as possible
            {
                if (iaf == null)
                    return;

                iaf.CopyInteractionDataTo(userInfos);
            }

            var hasUser = false;
            foreach (var userInfo in userInfos)
            {
                var userID = userInfo.SkeletonTrackingId;
                if (userID == 0)
                    continue;

                hasUser = true;
                var hands = userInfo.HandPointers;
                if (hands.Count == 0)
                    CapturedHands = new List<Hand>();
                else
                {
                    CapturedHands = new List<Hand>();
                    foreach (var hand in hands)
                    {
                        var lastHandEvents = hand.HandType == InteractionHandType.Left
                                                 ? lastLeftHandEvents
                                                 : lastRightHandEvents;

                        if (hand.HandEventType != InteractionHandEventType.None)
                            lastHandEvents[userID] = hand.HandEventType;

                        var lastHandEvent = lastHandEvents.ContainsKey(userID)
                                                ? lastHandEvents[userID]
                                                : InteractionHandEventType.None;

                        CapturedHands.Add(new Hand()
                        {
                            UserId = userInfo.SkeletonTrackingId,
                            HandType = hand.HandType,
                            HandEventType = hand.HandEventType,
                            IsGripping = InteractionHandEventType.Grip == lastHandEvent ? true:false,
                            LastHandEvent = lastHandEvent,
                            IsActive = hand.IsActive,
                            IsPrimaryForUser = hand.IsPrimaryForUser,
                            IsInteractive = hand.IsInteractive,
                            PressExtent = hand.PressExtent,
                            IsPressed = hand.IsPressed,
                            IsTracked = hand.IsTracked,
                            X = hand.X,
                            Y = hand.Y,
                            RawX = hand.RawX,
                            RawY = hand.RawY,
                            RawZ = hand.RawZ
                        });

                    }
                }
            }

            if (!hasUser) {
                CapturedHands = new List<Hand>();
            }
        }

    }
}
