﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RieeGames;

namespace RieeEngine.SceneManager.Screens
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Content;


    /// <summary>
    /// This class has everything that need to display a message on the screen, made a small fade out and the capability to change to a different game Screen.
    /// </summary>
    public class GameFinished : SceneManager.GameScreen
    {
        SpriteFont font;
        Texture2D texture;
        ContentManager content;
        int timeToTransition = 3000;
        GameScreen screenToChange;

        /// <summary>
        /// Starts a new instance
        /// </summary>
        /// <param name="screenToChange">The screen that you want to change</param>
        public GameFinished(GameScreen screenToChange)
        {
            this.screenToChange = screenToChange;
        }

        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            font = content.Load<SpriteFont>("RieeEngine/Segoe16");
            texture = content.Load<Texture2D>("heart");
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus,false);

            timeToTransition -= gameTime.ElapsedGameTime.Milliseconds;

            if (timeToTransition < 0)
                LoadingScreen.Load(ScreenManager, true, screenToChange);
        }
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();
            spriteBatch.Draw(texture, new Vector2((graphics.Viewport.Width - texture.Width) / 2, (graphics.Viewport.Height - texture.Height) / 2),
                    Color.White);
            spriteBatch.DrawString(this.font, "Aaa perdistes \n cargando siguiente actividad", new Vector2((graphics.Viewport.Width - texture.Width) / 2, (graphics.Viewport.Height - texture.Height) / 2), Color.Black);
            spriteBatch.End();

        }

    }
}
