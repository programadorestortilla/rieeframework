﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RieeEngine
{
    /// <summary>
    /// The entity class is a component that makes an Image capable of being drawn and updated in your game
    /// This class uses a Radius variable for circular collision handling.
    /// </summary>
    public abstract class Entity
    {
        public Vector2 position;
        public bool isExpired;
        public Rectangle entityRectangle;
        protected Texture2D image;
        protected Color color = Color.White;

        public Vector2 Size
        {
            get
            {
                return image == null ? Vector2.Zero : new Vector2(image.Width, image.Height);
            }
        }

        /// <summary>
        /// Updates your image or component.
        /// </summary>
        /// <param name="gameTime"></param>
        public abstract void update(GameTime gameTime);

        /// <summary>
        /// This one draws your image.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(image,position,color);
        }
    }


}
