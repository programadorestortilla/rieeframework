sampler sprite : register(s0);

static const int MinDepthVal = 300 << 3;

static const int MaxDepthVal = 4000 << 3;
// TODO: add effect parameters here.

static const float4 playerColorCoefficientsIndex[] = 
{
    float4(1.0,  1.0,  1.0,  1.0), 
    float4(1.0,  0.2,  0.2,  1.0),
    float4(0.2,  1.0,  0.2,  1.0),
    float4(0.2,  0.2,  1.0,  1.0),
    float4(1.0,  1.0,  0.2,  1.0),
    float4(1.0,  0.2,  1.0,  1.0),
    float4(0.2,  1.0,  1.0,  1.0)
};

float4 DepthToRGB(float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 color = tex2D(sprite, texCoord);

    color *= 15.01f;

    int4 iColor = (int4)color;
    int depthV = iColor.w * 4096 + iColor.x * 256 + iColor.y * 16 + iColor.z;

    int playerId = depthV % 8;

    float gray = ((float)depthV - MinDepthVal) / ((float)MaxDepthVal - MinDepthVal);

    return float4(gray, gray, gray, 1.0f) * playerColorCoefficientsIndex[playerId];
}

technique KinectDepthConversion
{
    pass KinectDepthConversion
    {
		// TODO: set renderstates here.
        PixelShader = compile ps_2_0 DepthToRGB();
    }
}
