sampler sprite : register(s0);
// TODO: add effect parameters here.

float4 BGRtoRGB(float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 tex = tex2D(sprite, texCoord);
    return tex.bgra;
}

technique KinectColorConversion
{
    pass KinectColorConversion
    {
		// TODO: set renderstates here.
        PixelShader = compile ps_2_0 BGRtoRGB();
    }
}
