using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RieeEngine.SceneManager;
using RieeEngine.Kinect;
using RieeEngine.Graphics;

namespace RieeGames
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        ScreenManager screenManager;
        SpriteBatch spriteBatch;

        /// <summary>
        /// This control selects a sensor, and displays a notice if one is
        /// not connected.
        /// </summary>
        private readonly KinectChooser chooser;

        /// <summary>
        /// This manages the rendering of the Color stream
        /// </summary>
        private readonly ColorStreamRenderer colorStream;

        private readonly DepthStreamRenderer depthStream;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
            graphics.PreparingDeviceSettings += GraphicsConfigurations.GraphicsDevicePreparingDeviceSettings;

            screenManager = new ScreenManager(this);
            Components.Add(screenManager);
            screenManager.AddScreen(new Activities.Colecciones.Main(0.2f, 0.2f, 60));

            //this.chooser = new KinectChooser(this,true,true);
            //this.chooser = new KinectChooser(this, false, true);
            this.chooser = new KinectChooser(this, new InteractionClient(), true);
            this.Services.AddService(typeof(KinectChooser), this.chooser);
            this.Components.Add(this.chooser);

            this.chooser.depthStreamRenderer.Position = new Vector2(180, 10);
            this.chooser.depthStreamRenderer.Size = new Vector2(160, 120);
            
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(this.GraphicsDevice);
            this.Services.AddService(typeof(SpriteBatch), this.spriteBatch);
            base.LoadContent();
        }

        protected override void Initialize()
        {
            
            base.Initialize();
        }


        

    }
}

namespace RieeGames
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Main game = new Main())
            {
                game.Run();
            }
        }
    }
#endif
}


