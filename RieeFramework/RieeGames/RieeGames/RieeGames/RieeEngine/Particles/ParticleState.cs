﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RieeEngine.Particles;
using RieeEngine;

namespace RieeGames.Activities.Colecciones
{

    public class ParticleState
    {
        public Vector2 Velocity;
        public float LengthMultiplier;

        public static void updateParticleExplosion(ParticleManager<ParticleState>.Particle particle)
        {
            var vel = particle.State.Velocity;
            particle.Position += vel;
            particle.Orientation = vel.toAngle();
            float speed = vel.Length();
            float alpha = Math.Min(1, Math.Min(particle.PercentLife * 2, speed * 1f));
            alpha *= alpha;
            //particle.Color.A = (byte)(255 * alpha);

            particle.Scale.X = particle.State.LengthMultiplier * Math.Min(Math.Min(1f, 0.2f * speed + 0.1f), alpha);
            particle.Scale.Y = particle.State.LengthMultiplier * Math.Min(Math.Min(1f, 0.2f * speed + 0.1f), alpha);
            // denormalized float cause significant performance issues

            if (Math.Abs(vel.X) + Math.Abs(vel.Y) < 0.00000000001f)
                vel = Vector2.Zero;

            var pos = particle.Position;
            int width = (int)1024;// checa esto
            int height = (int)768; // esto

            // collide with the edges of the screen
            if (pos.X < 0)
                vel.X = Math.Abs(vel.X);
            else if (pos.X > width)
                vel.X = -Math.Abs(vel.X);
            if (pos.Y < 0) // y aqui tambien!!
                vel.Y = Math.Abs(vel.Y);
            else if (pos.Y > height)
                vel.Y = -Math.Abs(vel.Y);

            vel *= 0.97f;       // particles gradually slow down
            particle.State.Velocity = vel;

        }

    }
}
