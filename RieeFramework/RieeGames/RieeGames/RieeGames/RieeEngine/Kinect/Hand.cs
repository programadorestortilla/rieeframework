﻿namespace RieeEngine.Kinect
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Kinect.Toolkit.Interaction;
    using Microsoft.Xna.Framework;
    using Microsoft.Kinect;

    public class Hand
    {
        public int UserId;
        public InteractionHandType HandType;
        public InteractionHandEventType HandEventType;
        public InteractionHandEventType LastHandEvent;
        public bool IsActive;
        public bool IsPrimaryForUser;
        public bool IsInteractive;
        public double PressExtent;
        public bool IsPressed;
        public bool IsTracked;
        public double X;
        public double Y;
        public double RawX;
        public double RawY;
        public double RawZ;
        public bool IsGripping;
    }
}
