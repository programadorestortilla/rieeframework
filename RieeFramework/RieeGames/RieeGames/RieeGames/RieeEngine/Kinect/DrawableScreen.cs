﻿namespace RieeEngine.Kinect
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public abstract class DrawableScreen:DrawableGameComponent
    {

        public Vector2 Size { get; set; }

        public Vector2 Position { get; set; }


        /// <summary>
        /// Gets the kinect selected by the chooser.
        /// </summary>
        public KinectChooser Chooser
        {
            get
            {
                return (KinectChooser)this.Game.Services.GetService(typeof(KinectChooser));
            }
        }

        /// <summary>
        /// Gets the Spritebatch from the Variables tree.
        /// </summary>
        public SpriteBatch SharedSpriteBatch
        {
            get
            {
                return (SpriteBatch)this.Game.Services.GetService(typeof(SpriteBatch));
            }
        }

        public DrawableScreen(Game game)
            : base(game)
        {
        }
    }
}
