﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RieeEngine.Kinect
{

    public class SkeletonStreamCatcher
    {

        private readonly KinectChooser Chooser;
        private static Skeleton[] skeletonData;

        public Vector2 leftHand{ get; private set;}
        public Vector2 rightHand { get; private set; }

        public SkeletonStreamCatcher(KinectChooser Chooser)
        {
            this.Chooser = Chooser;
            leftHand = Vector2.Zero;
            rightHand = Vector2.Zero;
        }

        public void update()
        {
            // If the sensor is not found, not running, or not connected, stop now
            if (null == this.Chooser.Sensor ||
                false == this.Chooser.Sensor.IsRunning ||
                KinectStatus.Connected != this.Chooser.Sensor.Status)
            {
                return;
            }

            using (var skeletonFrame = this.Chooser.Sensor.SkeletonStream.OpenNextFrame(0))
            {
                if (null == skeletonFrame)
                    return;

                // Reallocate if necessary
                if (null == skeletonData || skeletonData.Length != skeletonFrame.SkeletonArrayLength)
                {
                    skeletonData = new Skeleton[skeletonFrame.SkeletonArrayLength];
                }

                skeletonFrame.CopySkeletonDataTo(skeletonData);
            }

            if (skeletonData != null)
                separateSkeletonMembers(skeletonData);
        }

        private void separateSkeletonMembers(Skeleton[] data)
        {
            foreach (var skeleton in skeletonData)
            {
                switch(skeleton.TrackingState)
                {
                    case SkeletonTrackingState.Tracked:
                    leftHand = getMember(skeleton.Joints, JointType.HandLeft);
                    rightHand = getMember(skeleton.Joints, JointType.HandRight);
                    break;
                }
            }
        }

        private Vector2 getMember(JointCollection joints, JointType member)
        {
            return SkeletonToColorMap(joints[member].Position);
        }

        /// <summary>
        /// This method is used to map the SkeletonPoint to the color frame.
        /// </summary>
        /// <param name="point">The SkeletonPoint to map.</param>
        /// <returns>A Vector2 of the location on the color frame.</returns>
        private Vector2 SkeletonToColorMap(SkeletonPoint point)
        {
            if ((null != Chooser.Sensor) && (null != Chooser.Sensor.ColorStream))
            {
                // This is used to map a skeleton point to the color image location
                var colorPt = Chooser.Sensor.CoordinateMapper.MapSkeletonPointToColorPoint(point, Chooser.Sensor.ColorStream.Format);
                return new Vector2(colorPt.X, colorPt.Y);
            }

            return Vector2.Zero;
        }
    }
}
