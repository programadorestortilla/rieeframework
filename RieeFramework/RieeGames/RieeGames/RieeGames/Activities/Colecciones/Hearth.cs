﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RieeEngine;

namespace RieeGames.Activities.Colecciones
{
    class Hearth:Entity
    {
        private bool destroyed = false;
        private int framesUntilGone = 0;
        public Hearth(Texture2D texture, Vector2 position)
        {
            this.image = texture;
            this.position = position;
        }

        public override void update(GameTime gameTime)
        {
            if (destroyed)
            {
                framesUntilGone--;
                this.color = Color.White * (1 + framesUntilGone / 60f);
            }

            if (this.color.A == 0)
                this.isExpired = true;
        }

        public override void draw(SpriteBatch spriteBatch)
        {
            base.draw(spriteBatch);
        }

        public void destroy()
        {
            this.destroyed = true;
            //Deploy some particle effects or something
        }

    }
}
