﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RieeGames.Activities.Colecciones
{
    static class PlayerStatus
    {
        public static int lives { get; private set; }
        public static int score { get; set; }
        public static bool isGameOver { get { return lives == 0; } }

        public static void start()
        {
            score = 0;
            lives = 3;
        }

        public static void addPoints()
        {
            if (isGameOver)
                return;

            score++;
        }

        public static void removeLife()
        {
            if (isGameOver)
                return;

            lives--;
        }

        public static void reset()
        {
            score = 0;
            lives = 3;
        }

    }
}
