﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RieeEngine;
using RieeEngine.Particles;

namespace RieeGames.Activities.Colecciones
{
    public class Number:Entity
    {
        public int sizeOfCollection;
        public bool isReset{get; private set;}
        private Vector2 startingPosition{get; set;}
        private bool isActive;
        private int timeUntilStart = 60;
        private Random rand = new Random();
        private SpriteFont font; 

        public Number(Texture2D texture , Vector2 position, int size){
            sizeOfCollection = size;
            font = Font.Number;
            startingPosition = position;
            this.image = texture;
            this.position = startingPosition =  position;
        }

        public override void update(GameTime gameTime)
        {
            if (isReset)
            {
                timeUntilStart = 60;
                this.position = startingPosition;
                isReset = false;
            }
            entityRectangle = new Rectangle((int)this.position.X, (int)this.position.Y, this.image.Width, this.image.Height);
           // do nothing
            if (timeUntilStart <= 0)
                isActive = true;
            else
            {
                timeUntilStart--;
                this.color = Color.White * (1 - timeUntilStart / 60f);
            }
        }

        public override void draw(SpriteBatch spriteBatch)
        {
            //draw, duh
            spriteBatch.DrawString(font, "" + sizeOfCollection, position, Color.Black);

            if (timeUntilStart > 0)
            {
                // Draw an expanding, fading-out version of the sprite as part of the spawn-in effect.
                float factor = timeUntilStart / 60f;	// decreases from 1 to 0 as the entity spawns in
                spriteBatch.Draw(image, position, null, Color.White * factor, 0f, Size/5, 2 - factor, 0, 0);
            }
            base.draw(spriteBatch);
        }

        public void destroy()
        {
            this.isExpired = true;
            //Deploy some particle effects or something
            rand = new Random();

            EntityManager.add(new ScoreAnimationSprite(Art.Point, position, new Vector2(ScoreEntity.instance.position.X + (ScoreEntity.instance.entityRectangle.Width / 2), 0),0.6f, 60));
            for (int i = 0; i < 20; i++)
            {
                float speed = 10f * (1f - 1 / rand.nextFloat(1f, 10f));

                var state = new ParticleState()
                {
                    Velocity = rand.nextVector2(speed, speed),
                    LengthMultiplier = 1f
                };

                Color color = Color.White;
                Main.particleManager.CreateParticle(Art.Particle, new Vector2(position.X+(Art.Point.Width/2), position.Y), color, 100f, 1f, state);
            }
        }

        public void handleCollision(Entity other)
        {
            if (other is Pointer)
                return;
            if (isActive)
            {
                if (other is Collection)
                    if (sizeOfCollection == (other as Collection).sizeOfCollection)
                        destroy();
                    else
                    {
                        badAnswer();
                        Console.WriteLine("Choque con :" + other.GetType());
                    } 
            }
        }

        public void badAnswer()
        {
            position = startingPosition;
            entityRectangle = Rectangle.Empty;
            PlayerStatus.removeLife();
            isReset = true;
        }
    }
}
