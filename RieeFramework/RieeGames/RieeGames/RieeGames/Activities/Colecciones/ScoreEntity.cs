﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using RieeEngine;

namespace RieeGames.Activities.Colecciones
{
    public class ScoreEntity:Entity
    {
        private SpriteFont font;
        public static ScoreEntity instance;

        public ScoreEntity(Vector2 position)
        {
            instance = this;
            this.position = position;
            font = Font.Score;
            entityRectangle = new Rectangle((int)position.X, (int)position.Y, 400, 1);
        }

        public override void update(GameTime gameTime)
        {
            // do nothing
        }

        public override void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, "Score: " + PlayerStatus.score, position, color);
        }
    }
}
