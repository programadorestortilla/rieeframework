﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RieeEngine;


namespace RieeGames.Activities.Colecciones
{
    public static class EntityManager
    {
        static List<Entity> entities = new List<Entity>();
        private static List<Collection> collections = new List<Collection>();
        private static List<Number> numbers = new List<Number>();
        private static List<Hearth> hearths = new List<Hearth>();
        private static List<ScoreAnimationSprite> sprites = new List<ScoreAnimationSprite>();
        private static int lives = PlayerStatus.lives;
        public static int heartszie { get { return hearths.Count; } }

        public static void start()
        {
            lives = PlayerStatus.lives;
        }
        public static void add(Entity entity)
        {
            if (entity is Collection)
                collections.Add(entity as Collection);
            else if (entity is Number)
                numbers.Add(entity as Number);
            else if (entity is Hearth)
                hearths.Add(entity as Hearth);
            else if (entity is ScoreAnimationSprite)
                sprites.Add(entity as ScoreAnimationSprite);
            else
                entities.Add(entity);
        }
        
        public static void update(GameTime gameTime)
        {
            handleCollisions();
            checkLives();

            foreach (var entity in entities)
                entity.update(gameTime);

            foreach (var entity in collections)
                entity.update(gameTime);

            foreach (var entity in numbers)
                entity.update(gameTime);

            foreach (var entity in hearths)
                entity.update(gameTime);

            foreach (var entity in sprites)
                entity.update(gameTime);

            numbers = numbers.Where(x => !x.isExpired).ToList();
            collections = collections.Where(x => !x.isExpired).ToList();
            hearths = hearths.Where(x => !x.isExpired).ToList();
            sprites = sprites.Where(x => !x.isExpired).ToList();

            if (numbers.Count == 0 && collections.Count==0)
                CollectionFactory.creating = true;
            else
                CollectionFactory.creating = false;
        }

        public static void draw(SpriteBatch spriteBatch)
        {  
            foreach (var entity in collections)
                entity.draw(spriteBatch);
            foreach (var entity in numbers)
                entity.draw(spriteBatch);
            foreach (var entity in entities)
                entity.draw(spriteBatch);
            foreach (var entity in hearths)
                entity.draw(spriteBatch);
            foreach (var entity in sprites)
                entity.draw(spriteBatch);
        }

        private static void checkLives()
        {
            if (lives != PlayerStatus.lives)
            {
                Console.WriteLine("\nLives Manager: " + lives + " Lives PLayerStatus : " + PlayerStatus.lives);
                hearths[hearths.Count-1].destroy();
                lives--;   
            }
        }

        private static bool isColliding(Entity a , Entity b)
        {
            
            return a.entityRectangle.Intersects(b.entityRectangle);
        }

        private static void handleCollisions()
        {
            //handle collision between collections and numbers
            for (int i = 0; i < collections.Count; i++)
            {
                for (int j = 0; j < numbers.Count; j++)
                {
                    if (isColliding(collections[i], numbers[j]))
                    {
                        collections[i].handleCollision(numbers[j]);
                        numbers[j].handleCollision(collections[i]);
                        
                    }
                }
            }


            
            //handle collisions between numbers and the pointher
            for (int i = 0; i < numbers.Count; i++)
            {
                if (isColliding(numbers[i], entities[0]))
                {
                    numbers[i].handleCollision(entities[0]);
                    (entities[0] as Pointer).handleCollision(numbers[i]);
                }
            }

            //handle collisions between score animations and Score.
            for (int i = 0; i < sprites.Count; i++)
            {
                if (isColliding(sprites[i], ScoreEntity.instance))
                    sprites[i].handleCollision();
            }

        }

        public static void disposeElements()
        {
            entities = new List<Entity>();
            collections = new List<Collection>();
            numbers = new List<Number>();
            hearths = new List<Hearth>();
            sprites = new List<ScoreAnimationSprite>();
        }

    }
}
