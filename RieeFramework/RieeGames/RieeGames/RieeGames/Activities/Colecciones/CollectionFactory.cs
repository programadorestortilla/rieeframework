﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RieeEngine;
using RieeEngine.SceneManager;

namespace RieeGames.Activities.Colecciones
{
    public static class CollectionFactory
    {

        public static bool creating{private get; set;}
        public static int counts;
        private static List<int> numbersCreated;
        private static Random random = new Random();
        private static Vector2[] positions;

        public static void update()
        {
            if (creating) {
                counts++;
                numbersCreated = new List<int>();
                for (int i = 0; i < 3; i++)
                {
                    positions = getPositions();
                    int number;
                    if (i == 0)
                    {
                        number = random.Next(1,21);
                        numbersCreated.Add(number);
                        EntityManager.add(new Number(Art.Apple, getPositions()[0], number));
                        EntityManager.add(new Collection(Art.Box, new Vector2(50, 200), number));
                    }
                    else
                    {

                        do
                        {
                          number = random.Next(1, 21);
                        }while (numbersCreated.Contains(number));

                        numbersCreated.Add(number);

                        if (i == 2)
                        {
                            EntityManager.add(new Number(Art.Apple, getPositions()[1], number));
                            EntityManager.add(new Collection(Art.Box,new Vector2(50, 400), number));
                        }
                        else
                        {
                            EntityManager.add(new Number(Art.Apple, getPositions()[2], number));
                            EntityManager.add(new Collection(Art.Box, new Vector2(50,600), number));
                        }
                    }
                }
            }   
        }

        private static Vector2[] getPositions()
        {
            Vector2[] positions = new Vector2[3];
            Random random = new Random();
            int[] numbers = random.randomNoRepeatedArray(3);

            for (int i = 0; i < numbers.Count(); i++)
            {
                switch (numbers[i])
                {
                    case 0:
                        positions[i] = new Vector2(900, 200);
                        break;
                    case 1:
                        positions[i] = new Vector2(900, 400);
                        break;
                    case 2:
                        positions[i] = new Vector2(900, 600);
                        break;
                }
                    

            }

            return positions;
        }


    }
}
