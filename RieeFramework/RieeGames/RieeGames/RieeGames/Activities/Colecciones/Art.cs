﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace RieeGames.Activities.Colecciones
{
    public static class Art
    {
        public static Texture2D Pointer { get; private set; }
        public static Texture2D Header { get; private set; }
        public static Texture2D Apple { get; private set; }
        public static Texture2D Box { get; private set; }
        public static Texture2D Heart { get; private set; }
        public static Texture2D Point { get; private set; }
        public static Texture2D Particle { get; private set; }
        public static Texture2D ClosedHand { get; private set;}

        public static void load(ContentManager content)
        {
            Pointer = content.Load<Texture2D>("rightHand");
            Apple = content.Load<Texture2D>("apple");
            Box = content.Load<Texture2D>("Box");
            Heart = content.Load<Texture2D>("Heart");
            Header = content.Load<Texture2D>("goalV");
            Point = content.Load<Texture2D>("Point");
            Particle = content.Load<Texture2D>("star");
            ClosedHand = content.Load<Texture2D>("draghand");

        }

    }
}
