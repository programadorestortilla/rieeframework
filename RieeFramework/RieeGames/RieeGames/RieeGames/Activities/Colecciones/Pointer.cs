﻿namespace RieeGames.Activities.Colecciones
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.GamerServices;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;
    using RieeEngine;
    using RieeEngine.Kinect;
    using Microsoft.Kinect;

    class Pointer:Entity
    {
        
        public static Pointer Instance { get; private set; }
        private Entity items ;
        private KinectChooser kinectInput;
        private Vector2 movementScalation;
        private List<Hand> hands;
        private bool closedHand;

        public Pointer(KinectChooser kinect, Vector2 movementScalation)
        {
            this.image = Art.Pointer;
            this.position = Vector2.Zero;
            this.entityRectangle = new Rectangle((int)this.position.X, (int)this.position.Y, this.image.Width, this.image.Height);
            this.kinectInput = kinect;
            this.movementScalation = movementScalation;
            if (Instance == null)
                Instance = this;
        }

        public override void update(GameTime gameTime)
        {
            //this.position = Input.MousePosition;

            // If the sensor is not found, not running, or not connected, stop now
            if (null == this.kinectInput.Sensor ||
                false == this.kinectInput.Sensor.IsRunning ||
                KinectStatus.Connected != this.kinectInput.Sensor.Status)
            {
                return;
            }

            if (kinectInput.interactionStream == null || kinectInput.interactionStream.CapturedHands == null)
                return;

            if (kinectInput.interactionStream.CapturedHands.Count == 0)
                return;

            hands = kinectInput.interactionStream.CapturedHands;
            closedHand = hands.Last().IsGripping;

            this.position = new Vector2((float)hands.Last().X * 600, (float)hands.Last().Y * 600);
            
            this.entityRectangle = new Rectangle((int)this.position.X, (int)this.position.Y, this.image.Width, this.image.Height);

            if (items != null && hands.Last().IsGripping)
                items.position = position;

            if ((items != null && items.isExpired) || (items != null && (items as Number).isReset)||(items != null && !hands.Last().IsGripping) )
                items = null;
        }

        public override void draw(SpriteBatch spriteBatch)
        {
            if (closedHand)
                spriteBatch.Draw(Art.ClosedHand, position, color);
            else
                spriteBatch.Draw(image, position, color);
        }

        public void handleCollision(Entity other)
        {
            // Just in case ;)
            if (hands.Count == 0)
                return;

            if (other is Number && items == null)
                items = other;

        }
    }
}
