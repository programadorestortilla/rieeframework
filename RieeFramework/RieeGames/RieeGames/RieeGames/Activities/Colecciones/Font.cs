﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace RieeGames.Activities.Colecciones
{
    public static class Font
    {
        public static SpriteFont Number { get; private set; }
        public static SpriteFont Score { get; private set; }
        public static SpriteFont Time { get; private set; }

        public static void load(ContentManager content)
        {
            Number = content.Load<SpriteFont>("RieeEngine/loadingFont");
            Score = content.Load<SpriteFont>("score");
            Time = content.Load<SpriteFont>("Activities/Colecciones/timeFont");
        }

    }
}
