﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RieeEngine;
using RieeEngine.Particles;

namespace RieeGames.Activities.Colecciones
{
    public class Collection:Entity
    {
        public int sizeOfCollection;
        private int timeUntilStart = 60; // fps
        private bool isActive, collided;
   
        

        public Collection(Texture2D texture , Vector2 position, int size){
            sizeOfCollection = size;
            this.image = texture;
            this.position = position;
            this.color = Color.White * 0;

            if (sizeOfCollection > 9)
                this.entityRectangle = new Rectangle((int)position.X, (int)position.Y, (int)this.Size.X * 10, (int)this.Size.Y * 2);
            else
                this.entityRectangle = new Rectangle((int)position.X, (int)position.Y, (int)this.Size.X * sizeOfCollection, (int)this.Size.Y);
        }

        public override void update(GameTime gameTime)
        {
           // do nothing
            if (timeUntilStart <= 0)
                isActive = true;
            else
            {
                timeUntilStart--;
                this.color = Color.White * (1 - timeUntilStart / 60f);
            }

            if (collided)
            {
                timeUntilStart--;
                this.color = Color.White * (1 + timeUntilStart/ 60f);
                

                if (color.A <= 0)
                    isExpired = true;
            }
        }

        public override void draw(SpriteBatch spriteBatch)
        {
            if (sizeOfCollection > 9)
            {
                for (int i = 0; i < 9; i++)
                    spriteBatch.Draw(this.image, new Vector2(position.X + (i * this.image.Width), position.Y), color);
                for (int i = 9; i < sizeOfCollection; i++)
                    spriteBatch.Draw(this.image, new Vector2(position.X + (i * this.image.Width)- this.image.Width*8 - this.image.Width, position.Y + (this.image.Height)), color);
            }
            else
                for (int i = 0; i < sizeOfCollection; i++)
                    spriteBatch.Draw(this.image, new Vector2(position.X + (i * this.image.Width), position.Y), color);
        }

        public void destroy()
        {
            collided = true;
            //Deploy some particle effects
        }

     
        public void handleCollision(Entity other)
        {
            if (isActive)
                if ((other as Number).sizeOfCollection == this.sizeOfCollection)
                    destroy();
        }
    }
}
