﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RieeEngine;
using RieeEngine.Particles;

namespace RieeGames.Activities.Colecciones
{
    class ScoreAnimationSprite:Entity
    {
        private int timeUntilStart;
        private Vector2 destination;
        private float speed,speedI,fps,deformation;

        /// <summary>
        /// Constructor to initialize the Animated deformable entity
        /// </summary>
        /// <param name="texture">The texture that the entity will have</param>
        /// <param name="position">The starting position</param>
        /// <param name="destination">The destination of the trayectory</param>
        /// <param name="speed">The speed that it will move.</param>
        /// <param name="fps">The amount of frames that the starting animation will take to render</param>
        public ScoreAnimationSprite(Texture2D texture, Vector2 position, Vector2 destination, float speed,int timeUntilStart)
        {
            this.image = texture;
            this.position = position;
            this.destination = destination;
            this.speed = speedI=  speed;
            this.timeUntilStart = timeUntilStart;
            fps = timeUntilStart;
            entityRectangle = new Rectangle((int)position.X, (int)position.Y,image.Height, 0);
        }

        public override void update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.Milliseconds;
            timeUntilStart--;
            if (timeUntilStart <= 0)
            {
                float x = destination.X - position.X;
                float y = destination.Y - position.Y;
                Vector2 vector = new Vector2(x, y);
                vector.Normalize();
                //destination.Normalize();
                position += (vector * speed) * elapsed;
                //entityRectangle = new Rectangle((int)this.position.X, (int)this.position.Y, this.image.Width, this.image.Height);
                entityRectangle.X = (int)position.X;
                entityRectangle.Y = (int)position.Y;
                color = Color.White * (Vector2.Distance(position, destination) * 0.1f / fps);
            }
            else
            {
                color = Color.White * (1 - timeUntilStart / fps);
                deformation += (speedI) * elapsed;
                if (timeUntilStart > fps / 2)
                    entityRectangle.Inflate(0, (int)(0.2 * elapsed));
                else
                    entityRectangle.Inflate(0, -(int)(0.1 * elapsed));
            }
        }

        public override void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(image, entityRectangle, color);
        }

        public void handleCollision()
        {
            PlayerStatus.addPoints();
            this.isExpired = true;

            
        }
    }
}
