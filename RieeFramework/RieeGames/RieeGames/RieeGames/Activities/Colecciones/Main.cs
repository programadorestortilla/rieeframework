﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RieeEngine.SceneManager;
using Microsoft.Kinect;
using RieeEngine.Kinect;
using RieeEngine.Particles;
using RieeEngine;

namespace RieeGames.Activities.Colecciones
{
    public class Main : GameScreen
    {
        #region VARIABLES
        ContentManager content;
        ScoreEntity score;
        Vector2 heartsPosition = new Vector2(500, 10);
        SpriteFont font;
        public static ParticleManager<ParticleState> particleManager { get; private set; }
        public KinectChooser kinectInput
        {
            get
            {
                return (KinectChooser)ScreenManager.Game.Services.GetService(typeof(KinectChooser));
            }
        }

        private float pauseAlpha;
        private float timerCount=0;
        #endregion

        #region CONFIGURATION VARIABLES
        Vector2 movementScalation;
        int timeLimit;
        #endregion
        public Main(float scaleX, float scaleY, int timeLimit)
        {
            movementScalation = new Vector2(scaleX, scaleY);
            this.timeLimit = timeLimit;
        }

        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            Art.load(content);
            Font.load(content);
            font = Font.Time;
            particleManager = new ParticleManager<ParticleState>(100, ParticleState.updateParticleExplosion);
            score = new ScoreEntity(new Vector2(280,20));
            PlayerStatus.start();
            EntityManager.start();
            EntityManager.add(new Pointer(kinectInput, movementScalation));
            for (int i = 1; i < PlayerStatus.lives+1; i++)
                EntityManager.add(new Hearth(Art.Heart, new Vector2(heartsPosition.X+(Art.Heart.Width*i),10)));
            ScreenManager.Game.ResetElapsedTime();
        }

        public override void UnloadContent()
        {
            PlayerStatus.reset();
            EntityManager.disposeElements();
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);
            
            if (kinectInput.Sensor == null || kinectInput.LastStatus != KinectStatus.Connected)
                return;

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (PlayerStatus.isGameOver || timeLimit <= 0)
            {
                ScreenManager.AddScreen(new RieeEngine.SceneManager.Screens.GameFinished(new RieeGames.Activities.Colecciones.Main(0.2f, 0.2f, 60)));
                return;
            }

            Input.update();
            timerCount += gameTime.ElapsedGameTime.Milliseconds;

            if (timerCount >= 1000)
            {
                timerCount = 0;
                timeLimit--;
            }

                //stringculero = " X  :" + Pointer.Instance.position.X + " Y : " + Pointer.Instance.position.Y +"\n time: "+timeLimit;

                /* Vector2 position = Input.MousePosition;
                 if (position.X >= (1024 - Pointer.Instance.Size.X))
                     position.X = (1024 - Pointer.Instance.Size.X);
                 else if (position.X <= 0)
                     position.X = 0;
                 if (position.Y <= 100)
                     position.Y = 100;
                 else if (position.Y >= (768 - Pointer.Instance.Size.Y))
                     position.Y = (768 - Pointer.Instance.Size.Y);

                Pointer.Instance.position = position;
                Input.update();*/
                score.update(gameTime);
                particleManager.Update();
                EntityManager.update(gameTime);
                CollectionFactory.update();

        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();
            spriteBatch.Draw(Art.Header, new Rectangle(0,0, Art.Header.Width, Art.Header.Height+40), Color.White);
            EntityManager.draw(spriteBatch);
            score.draw(spriteBatch);
            particleManager.Draw(spriteBatch);
            spriteBatch.DrawString(font, "Time : "+ timeLimit,
                new Vector2(graphics.Viewport.Width - 100, graphics.Viewport.Height - 30), Color.White);
            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }

        }
    }
}
